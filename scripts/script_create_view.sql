-- creacion de vista
	create or replace view v_empleados as
    select a.id_empldo
		, a.nmro_idntfccion
		, a.id_tpo_idntfccion
		, c.dscrpcion desc_tpo_idntfccion
		, a.prmer_nmbre
		, a.otro_nmbre
		, a.prmer_aplldo
		, a.sgndo_aplldo
		, a.email
		, a.id_area
		, d.dscrpcion nombre_area
		, a.id_pais
		, b.dscrpcion nombre_pais
		, a.fcha_ingrso
		, a.fcha_rgstro
		, a.fha_edcion
		, a.estado
	from empleados 				a
	join paises    				b on a.id_pais 				= b.id_pais	
	join tipo_identificaciones	c on a.id_tpo_idntfccion	= c.id_tpo_idntfccion
	join areas					d on a.id_area				= d.id_area	
    order by  a.id_empldo asc;
