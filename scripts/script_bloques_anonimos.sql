/*****************************************************************/
	--VERSION = 1.0
	--FECHA : 28/03/2021
  -- CREACION DE BLOQUES PARA EJECUTAR PROCEDIMIENTO Y FUNCIONES
  -- JOHN CAMILO RANGEL DUQUE
/*******************************************************************/

/************** BLOQUE PARA REGISTRAR EMPLEADO *****************/
DECLARE
  P_PRMER_APLLDO VARCHAR2(200);
  P_SGNDO_APLLDO VARCHAR2(200);
  P_PRMER_NMBRE VARCHAR2(200);
  P_OTRO_NMBRE VARCHAR2(200);
  P_ID_PAIS NUMBER;
  P_ID_TPO_IDNTFCCION NUMBER;
  P_NMRO_IDNTFCCION VARCHAR2(200);
  P_FCHA_INGRSO DATE;
  P_ID_AREA NUMBER;
  COD_RSPTA NUMBER;
  MNSJE VARCHAR2(200);
BEGIN
  P_PRMER_APLLDO := 'DE LA CALLE';
  P_SGNDO_APLLDO := 'FONSECA';
  P_PRMER_NMBRE := 'RAUL';
  P_OTRO_NMBRE :=  NULL;
  P_ID_PAIS := 1;
  P_ID_TPO_IDNTFCCION := 2;
  P_NMRO_IDNTFCCION := '104574225';
  P_FCHA_INGRSO := '28/01/2021';
  P_ID_AREA := 1;

  PKG_GESTION_EMPLEADOS.PRC_REGISTRO_EMPLEADOS(
    P_PRMER_APLLDO => P_PRMER_APLLDO,
    P_SGNDO_APLLDO => P_SGNDO_APLLDO,
    P_PRMER_NMBRE => P_PRMER_NMBRE,
    P_OTRO_NMBRE => P_OTRO_NMBRE,
    P_ID_PAIS => P_ID_PAIS,
    P_ID_TPO_IDNTFCCION => P_ID_TPO_IDNTFCCION,
    P_NMRO_IDNTFCCION => P_NMRO_IDNTFCCION,
    P_FCHA_INGRSO => P_FCHA_INGRSO,
    P_ID_AREA => P_ID_AREA,
    COD_RSPTA => COD_RSPTA,
    MNSJE => MNSJE
  );
  
DBMS_OUTPUT.PUT_LINE('COD_RSPTA = ' || COD_RSPTA); 
DBMS_OUTPUT.PUT_LINE('MNSJE = ' || MNSJE);

--rollback; 
END;

/***************** BLOQUE PARA ACTUALIZAR EMPLEADO **********************/
DECLARE
  P_ID_EMPLDO NUMBER;
  P_PRMER_APLLDO VARCHAR2(200);
  P_SGNDO_APLLDO VARCHAR2(200);
  P_PRMER_NMBRE VARCHAR2(200);
  P_OTRO_NMBRE VARCHAR2(200);
  P_ID_PAIS NUMBER;
  P_ID_TPO_IDNTFCCION NUMBER;
  P_NMRO_IDNTFCCION VARCHAR2(200);
  P_FCHA_INGRSO DATE;
  P_ID_AREA NUMBER;
  COD_RSPTA NUMBER;
  MNSJE VARCHAR2(200);
BEGIN
  P_ID_EMPLDO := 41; -- CONSULTAR EL ID DEL EMPLEADO ANTES DE ACTUALIZAR
  P_PRMER_APLLDO := 'CASTRO';
  P_SGNDO_APLLDO := 'FONSECA';
  P_PRMER_NMBRE := 'RAUL';
  P_OTRO_NMBRE := NULL;
  P_ID_PAIS := 1;
  P_ID_TPO_IDNTFCCION := 2;
  P_NMRO_IDNTFCCION := '104574225';
  P_FCHA_INGRSO := '28/03/21';
  P_ID_AREA := 1;

  PKG_GESTION_EMPLEADOS.PRC_AC_EMPLEADO(
    P_ID_EMPLDO => P_ID_EMPLDO,
    P_PRMER_APLLDO => P_PRMER_APLLDO,
    P_SGNDO_APLLDO => P_SGNDO_APLLDO,
    P_PRMER_NMBRE => P_PRMER_NMBRE,
    P_OTRO_NMBRE => P_OTRO_NMBRE,
    P_ID_PAIS => P_ID_PAIS,
    P_ID_TPO_IDNTFCCION => P_ID_TPO_IDNTFCCION,
    P_NMRO_IDNTFCCION => P_NMRO_IDNTFCCION,
    P_FCHA_INGRSO => P_FCHA_INGRSO,
    P_ID_AREA => P_ID_AREA,
    COD_RSPTA => COD_RSPTA,
    MNSJE => MNSJE
  );
 
DBMS_OUTPUT.PUT_LINE('COD_RSPTA = ' || COD_RSPTA);

DBMS_OUTPUT.PUT_LINE('MNSJE = ' || MNSJE);

--rollback; 
END;

/*********** BLOQUE PARA ELIMINAR EMPLEADO*********************/

DECLARE
  P_ID_EMPLDO NUMBER;
  COD_RSPTA NUMBER;
  MNSJE VARCHAR2(200);
BEGIN
  P_ID_EMPLDO := 41;

  PKG_GESTION_EMPLEADOS.PRC_EL_EMPLEADO(
    P_ID_EMPLDO => P_ID_EMPLDO,
    COD_RSPTA => COD_RSPTA,
    MNSJE => MNSJE
  );

DBMS_OUTPUT.PUT_LINE('COD_RSPTA = ' || COD_RSPTA);

DBMS_OUTPUT.PUT_LINE('MNSJE = ' || MNSJE);


--rollback; 
END;

/************ BLOQUE PARA CONSULTAR EMPLEADOS*******************/ 

DECLARE
  P_PRMER_APLLDO VARCHAR2(200);
  P_SGNDO_APLLDO VARCHAR2(200);
  P_PRMER_NMBRE VARCHAR2(200);
  P_OTRO_NMBRE VARCHAR2(200);
  P_ID_TPO_IDNTFCCION NUMBER;
  P_NMRO_IDNTFCCION VARCHAR2(200);
  P_ID_PAIS NUMBER;
  P_EMAIL VARCHAR2(200);
  P_ESTADO VARCHAR2(200);
  v_CURSOR_EMP sys_refcursor;
  COD_RSPTA NUMBER;
  MNSJE VARCHAR2(200);
  v_id_empldo       		NUMBER;
  v_prmer_aplldo    		VARCHAR2(200);
  v_sgndo_aplldo   	 		VARCHAR2(200);
  v_prmer_nmbre				VARCHAR2(200);
  v_otro_nmbre				VARCHAR2(200);
  v_id_pais					NUMBER;
  v_nombre_pais				VARCHAR2(200);
  v_id_tpo_idntfccion 		NUMBER;
  v_desc_tpo_idntfccion 	VARCHAR2(200);
  v_nmro_idntfccion 		VARCHAR2(200);
  v_email					VARCHAR2(200);
  v_fcha_ingrso 			VARCHAR2(200);
  v_id_area 				NUMBER;
  v_nombre_area 			VARCHAR2(200);
  v_fcha_rgstro 			DATE;
  
  
BEGIN
  P_PRMER_APLLDO := NULL;
  P_SGNDO_APLLDO := NULL;
  P_PRMER_NMBRE := NULL;
  P_OTRO_NMBRE := NULL;
  P_ID_TPO_IDNTFCCION := NULL;
  P_NMRO_IDNTFCCION := NULL;
  P_ID_PAIS := NULL;
  P_EMAIL := NULL;
  P_ESTADO := NULL;

  PKG_GESTION_EMPLEADOS.PRC_CN_EMPLEADO(
    P_PRMER_APLLDO => P_PRMER_APLLDO,
    P_SGNDO_APLLDO => P_SGNDO_APLLDO,
    P_PRMER_NMBRE => P_PRMER_NMBRE,
    P_OTRO_NMBRE => P_OTRO_NMBRE,
    P_ID_TPO_IDNTFCCION => P_ID_TPO_IDNTFCCION,
    P_NMRO_IDNTFCCION => P_NMRO_IDNTFCCION,
    P_ID_PAIS => P_ID_PAIS,
    P_EMAIL => P_EMAIL,
    P_ESTADO => P_ESTADO,
    CURSOR_EMP => v_CURSOR_EMP,
    COD_RSPTA => COD_RSPTA,
    MNSJE => MNSJE
  );
    
  loop fetch v_CURSOR_EMP into  v_id_empldo      	 
                                ,v_prmer_aplldo    	
                                ,v_sgndo_aplldo   	 	
                                ,v_prmer_nmbre			
                                ,v_otro_nmbre			
                                ,v_id_pais				
                                ,v_nombre_pais			
                                ,v_id_tpo_idntfccion 	
                                ,v_desc_tpo_idntfccion
                                ,v_nmro_idntfccion 	
                                ,v_email				
                                ,v_fcha_ingrso 		
                                ,v_id_area 			
                                ,v_nombre_area 		
                                ,v_fcha_rgstro 	;	
															
        exit when v_CURSOR_EMP%notfound; 
      
  
    DBMS_OUTPUT.PUT_LINE( v_id_empldo  
						||'-'||v_prmer_aplldo 
						||'-'|| v_sgndo_aplldo  
						||'-'|| v_prmer_nmbre	
						||'-'||  v_otro_nmbre	
						||'-'|| v_id_pais 
						||'-'|| v_nombre_pais	
						||'-'||	v_id_tpo_idntfccion 
	                    ||'-'||v_desc_tpo_idntfccion
	                    ||'-'||v_nmro_idntfccion 	
	                    ||'-'||v_email				
	                    ||'-'||v_fcha_ingrso 		
	                    ||'-'||v_id_area 			
	                    ||'-'||v_nombre_area 		
	                    ||'-'||v_fcha_rgstro );		
   end loop;
   close v_CURSOR_EMP;

DBMS_OUTPUT.PUT_LINE('COD_RSPTA = ' || COD_RSPTA);
 
DBMS_OUTPUT.PUT_LINE('MNSJE = ' || MNSJE);

END;
