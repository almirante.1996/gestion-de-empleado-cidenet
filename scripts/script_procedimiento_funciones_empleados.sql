/***************************************************************
  VERSION 1.0 
  FECHA : 30/03/2021
  CREACION DE PROCEDIMIENTOS Y FUNCIONES
  JOHN CAMILO RANGEL DUQUE
 **************************************************************************/	

------ HEADER  ---------------
create or replace package  pkg_gestion_empleados as
	 procedure prc_registro_empleados 	( p_prmer_aplldo 		in varchar2,
                                          p_sgndo_aplldo 		in varchar2,
                                          p_prmer_nmbre	 		in varchar2,
                                          p_otro_nmbre	 		in varchar2,
                                          p_id_pais		 		in number,	
                                          p_id_tpo_idntfccion	in number,
                                          p_nmro_idntfccion		in varchar2,
                                          p_fcha_ingrso			in date,
                                          p_id_area				in number,
                                          cod_rspta				out number,
                                          mnsje					out varchar2
                                        );

	 procedure  prc_ac_empleado ( p_id_empldo			in number,
                                  p_prmer_aplldo 		in varchar2,
                                  p_sgndo_aplldo 		in varchar2,
                                  p_prmer_nmbre	 		in varchar2,
                                  p_otro_nmbre	 		in varchar2,
                                  p_id_pais		 		in number,	
                                  p_id_tpo_idntfccion	in number,
                                  p_nmro_idntfccion		in varchar2,
                                  p_fcha_ingrso			in date,
                                  p_id_area				in number,
                                  cod_rspta				out number,
                                  mnsje					out varchar2
                                );

	 procedure prc_el_empleado ( p_id_empldo 	in number
                                ,cod_rspta		out number
                                ,mnsje			out varchar2
                                 );

	 procedure prc_cn_empleado ( p_prmer_aplldo 		in varchar2,
                                 p_sgndo_aplldo 		in varchar2,
                                 p_prmer_nmbre			in varchar2,
                                 p_otro_nmbre			in varchar2,
                                 p_id_tpo_idntfccion	in number,
                                 p_nmro_idntfccion		in varchar2,
                                 p_id_pais 				in number,
                                 p_email				in varchar2,
                                 p_estado				in varchar2,
                                 cursor_emp				out sys_refcursor,
                                 cod_rspta				out number,
                                 mnsje					out varchar2);
end ;
--- BODY ---------------------
-- procedimiento que permite guardar un empleado

create or replace package  body pkg_gestion_empleados as

	  procedure prc_registro_empleados 	( p_prmer_aplldo 		in varchar2,
                                          p_sgndo_aplldo 		in varchar2,
                                          p_prmer_nmbre	 		in varchar2,
                                          p_otro_nmbre	 		in varchar2,
                                          p_id_pais		 		in number,	
                                          p_id_tpo_idntfccion	in number,
                                          p_nmro_idntfccion		in varchar2,
                                          p_fcha_ingrso			in date,
                                          p_id_area				in number,
                                          cod_rspta				out number,
                                          mnsje					out varchar2
                                        )
	as
		
	-- declaramos todas las variables
		
		v_id_empldo 	  number;
		v_dscrpcion_dmnio varchar2(20);
		v_email			  varchar2(300); 		
		v_count			  number; 				
		
	
	begin
        cod_rspta := 0;
        mnsje    := 'Registro exitoso';  
		-- Buscamos si ese empleado ya existe en el sistema
		begin
			select id_empldo
			  into v_id_empldo		
			  from empleados
			 where id_tpo_idntfccion 	= p_id_tpo_idntfccion
			   and nmro_idntfccion 		= p_nmro_idntfccion;
			
			-- Si encuentró a un empleado retorno un mensaje.	
			cod_rspta := 10;	
			mnsje	  := 'IN_EMP '||cod_rspta||' Empleado ya se encuentra registrado';  
			return;
			
			--manejo de excepciones
		exception
			
			--Si no encuentra registro en el sistema comenzamos hacer el insert
			when no_data_found then
				
				--buscamos el dominio para asignarle al empleado
				begin
					select b.dscrpcion
					  into v_dscrpcion_dmnio  	
					  from paises   a 
					  join dominios	b on a.id_dmnio = b.id_dmnio
					where a.id_pais  = 	p_id_pais; 	
				end;
				
				--Construimos el correo del empleado
				v_email := lower(p_prmer_nmbre)||'.'||lower(replace(p_prmer_aplldo,' ',''))||'@'||v_dscrpcion_dmnio;
				
				--buscamos si existe correos iguales en tabla empleado
				begin
					select count(email)
					  into  v_count	
					  from empleados   
					 where email  = v_email;
					
					if v_count > 0 then	
						v_count  := v_count+1;	
						v_email := lower(p_prmer_nmbre)||'.'||lower(replace(p_prmer_aplldo,' ',''))||'.'||v_count||'@'||v_dscrpcion_dmnio;
					end if; 
					exception 	
						when others then
							cod_rspta	:= 30;
							mnsje		:= 'IN_EMP '||cod_rspta||' Error al generar correo electronico';	
							rollback;
							return;			
				end;
				
				-- validamos la fecha de ingreso
				if( (p_fcha_ingrso <= sysdate) and ( p_fcha_ingrso >= ADD_MONTHS(sysdate,-1)) ) then
                    -- Validamos Exprecion
					 if not( (regexp_like(p_prmer_aplldo,'^[ABCDEFGHIJKLMNQRSTQXWYZ][^ñÑáéíóúüÁÉÍÓÚÜ]+$') and regexp_like(p_sgndo_aplldo,'^[ABCDEFGHIJKLMNQRSTQXWYZ][^ñÑáéíóúüÁÉÍÓÚÜ]+$')) 
                         and (regexp_like(p_prmer_nmbre,'^[ABCDEFGHIJKLMNQRSTQXWYZ][^ñÑáéíóúüÁÉÍÓÚÜ]+$')  and regexp_like(p_otro_nmbre,'^[ABCDEFGHIJKLMNQRSTQXWYZ][^ñÑáéíóúüÁÉÍÓÚÜ]+$'))) then
                              
                            cod_rspta	:= 40;
                            mnsje		:= 'IN_EMP '||cod_rspta||' Valide campos, no se permite acentos ni letra Ñ.';	
                            return;		
                        else       
                             --insertamos en la tabla de empleados
                            begin
                                insert into empleados (prmer_aplldo, 		sgndo_aplldo,	prmer_nmbre, 	otro_nmbre, 	id_pais,	id_tpo_idntfccion, 
                                                       nmro_idntfccion, 	email, 		 	fcha_ingrso, 	id_area, 		estado,		fcha_rgstro, 			fha_edcion)
                                                values(p_prmer_aplldo,		p_sgndo_aplldo,	p_prmer_nmbre,	p_otro_nmbre,	p_id_pais,	p_id_tpo_idntfccion,
                                                       p_nmro_idntfccion,	v_email,		p_fcha_ingrso,	p_id_area,		'S',		sysdate,				null);
                                commit;
                                exception 
                                    when others then 
                                        cod_rspta := 50;	
                                        mnsje	  := 'IN_EMP '||cod_rspta||' Fallo al registrar el empleado'|| sqlerrm;  
                            end;
                     end if;   
                    
				else
					cod_rspta := 60;	
					mnsje	  := 'IN_EMP '||cod_rspta||' Fallo al registrar la fecha de ingreso debe estar entre '||trunc(sysdate)|| ' y '||trunc(ADD_MONTHS(sysdate,-1));  
					return;
				end if;
				
		end;
		
	end prc_registro_empleados; -- Fin del procedimiento

-- Procedimiento para actualizar a un empleado
	 procedure  prc_ac_empleado (p_id_empldo			in number,
                                  p_prmer_aplldo 		in varchar2,
                                  p_sgndo_aplldo 		in varchar2,
                                  p_prmer_nmbre	 		in varchar2,
                                  p_otro_nmbre	 		in varchar2,
                                  p_id_pais		 		in number,	
                                  p_id_tpo_idntfccion	in number,
                                  p_nmro_idntfccion		in varchar2,
                                  p_fcha_ingrso			in date,
                                  p_id_area				in number,
                                  cod_rspta				out number,
                                  mnsje					out varchar2)
	as

	--declaracion de varibale 
		
		v_id_pais 			number;	
		v_prmer_aplldo		varchar2(20);
		v_prmer_nmbre		varchar2(20);
		v_dscrpcion_dmnio	varchar2(20);
		v_email			    varchar2(300);	
		v_count             number;
		-- Validamos los constraint tipo check
		validate_check exception;
		pragma exception_init(validate_check,-02290);
		
	begin
        cod_rspta 			:= 0;
        mnsje    := 'Actualización exitoso';
		--consultamos si existe el empleado en el sistema.
		begin
			select id_pais
				   ,prmer_aplldo
				   ,prmer_nmbre	
			 into	v_id_pais
				   ,v_prmer_aplldo
				   ,v_prmer_nmbre
			from empleados
			where id_empldo = p_id_empldo;
		exception 
			when no_data_found then
				cod_rspta	:= 10;
				mnsje		:= 'AC_EMP '||cod_rspta||' Empleado no encontrado en el sistema.';
		end;

		--Generacion de nuevo correo electronico 
		if(v_prmer_nmbre != p_prmer_nmbre) or (v_prmer_aplldo != p_prmer_aplldo) or (v_id_pais != p_id_pais) then
				
			--consultamos el dominio nuevamente 
			begin
				select b.dscrpcion
				  into v_dscrpcion_dmnio  	
				  from paises   a 
				  join dominios	b on a.id_dmnio = b.id_dmnio
				where a.id_pais  = 	nvl(p_id_pais,v_id_pais); 
			exception	
				when others then
					cod_rspta	:= 20;
					mnsje		:= 'AC_EMP '||cod_rspta||' No se pudo generar Dominio';	
					rollback;
					return;
				end;	
			
			--Generamos correo
			v_email	:=  lower(nvl(p_prmer_nmbre,v_prmer_nmbre))||'.'||replace(lower(nvl(p_prmer_aplldo,v_prmer_aplldo)),' ','')||'@'||v_dscrpcion_dmnio;
			
			--consultamos si existe el correo iguales
			begin
				select count(email) cant
				  into v_count
				  from empleados
				 where email = v_email;

				if v_count > 0 then
					v_count := v_count + 1;
					v_email := lower(nvl(p_prmer_nmbre,v_prmer_nmbre))||'.'||replace(lower(nvl(p_prmer_aplldo,v_prmer_aplldo)),' ','')||'.'||v_count||'@'||v_dscrpcion_dmnio;
				end if;	
			exception 	
				when others then
					cod_rspta	:= 30;
					mnsje		:= 'AC_EMP '||cod_rspta||' Error al generar correo electronico';	
					rollback;
					return;
			end;	
		end if;
		--Actualizamos el empleado		
		begin 
			update empleados set  prmer_aplldo 		= nvl(p_prmer_aplldo,prmer_aplldo),
								  sgndo_aplldo 		= nvl(p_sgndo_aplldo,sgndo_aplldo),
								  prmer_nmbre	 	= nvl(p_prmer_nmbre,prmer_nmbre),		
								  otro_nmbre	 	= nvl(p_otro_nmbre,otro_nmbre),	
								  id_pais		 	= nvl(p_id_pais,id_pais),	
								  id_tpo_idntfccion	= nvl(p_id_tpo_idntfccion,id_tpo_idntfccion),
								  nmro_idntfccion	= nvl(p_nmro_idntfccion,nmro_idntfccion),	
								  fha_edcion		= sysdate,	
								  id_area			= nvl(p_id_area,id_area),
								  email				= nvl(v_email,email)
			where id_empldo = p_id_empldo;	
			commit;
		exception
			--Excepcion que me permite manejar los los contraint tipo check y cumplan con las validaciones de las expreciones.
			when validate_check then 
				cod_rspta := 40;
				mnsje	  := 'AC_EMP '|| cod_rspta || 'No cumple con las validaciones necesarias.'; 	
				rollback;
				return; 	
				
			when others then 
				cod_rspta	:= 50;
				mnsje		:= 'AC_EMP '||cod_rspta||' Error al actualizar al empleado'||sqlerrm;	
				rollback;
				return;
		end;
	end prc_ac_empleado; --Fin del procedimiento

	 procedure prc_el_empleado (p_id_empldo 	in number
                                ,cod_rspta		out number
                                ,mnsje			out varchar2
                                 )
	as
		--Declaramos las varibales
	
	begin
        	cod_rspta := 0;
            mnsje := 'Empleado elimando corectamente';
		begin
			delete 
			  from empleados 
			 where id_empldo = p_id_empldo;
			commit;
		exception 
			when others then 
				cod_rspta	:= 10;
				mnsje		:= 'EL_EMP '||cod_rspta||' Error al eliminar al empleado'||sqlerrm;	
				rollback;
				return;
		end;
	end;

	-- Procedimineto para buscar a los empleados

	 procedure prc_cn_empleado ( p_prmer_aplldo 		in varchar2,
                                 p_sgndo_aplldo 		in varchar2,
                                 p_prmer_nmbre			in varchar2,
                                 p_otro_nmbre			in varchar2,
                                 p_id_tpo_idntfccion	in number,
                                 p_nmro_idntfccion		in varchar2,
                                 p_id_pais 				in number,
                                 p_email				in varchar2,
                                 p_estado				in varchar2,
                                 cursor_emp				out sys_refcursor,
                                 cod_rspta				out number,
                                 mnsje					out varchar2)
	as
		     
	begin
        cod_rspta := 0;
        mnsje     := 'Consulta realizada con éxito';         
        begin 
            open cursor_emp for
                select id_empldo,
                       prmer_aplldo,
                       sgndo_aplldo,
                       prmer_nmbre,
                       otro_nmbre,
                       id_pais,
                       nombre_pais,
                       id_tpo_idntfccion,
                       desc_tpo_idntfccion,
                       nmro_idntfccion,
                       email,
                       fcha_ingrso,
                       id_area,
                       nombre_area,
                       fcha_rgstro
                from v_empleados	
                where prmer_aplldo 		like '%'||upper(p_prmer_aplldo)||'%' or prmer_aplldo is null
                and	  sgndo_aplldo		like '%'||upper(p_sgndo_aplldo)||'%' or p_sgndo_aplldo is null
                and	  prmer_nmbre		like '%'||upper(p_sgndo_aplldo)||'%' or p_prmer_nmbre is null
                and	  otro_nmbre		like '%'|| (upper(p_otro_nmbre))||'%' or p_otro_nmbre is null
                and	  id_tpo_idntfccion = nvl(p_id_tpo_idntfccion,id_tpo_idntfccion)
                and	  nmro_idntfccion	= upper(p_nmro_idntfccion) or p_nmro_idntfccion is null	
                and	  id_pais			= nvl(p_id_pais,id_pais)
                and	  email				like '%'||upper(p_email)||'%' or p_email is null
                and	  estado			like '%'||upper(p_estado)||'%' or p_estado is null;
              
            exception
                when others then 
                    cod_rspta:= 10;
                    mnsje:= cod_rspta||' - No se realizo  empelado, debido a: ' || sqlerrm;       
        end;
	end prc_cn_empleado;

end pkg_gestion_empleados;