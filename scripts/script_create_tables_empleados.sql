/***************************************************************
  VERSION 1.0 
  FECHA : 30/03/2021
  CREACION DE TABLAS GESTION DE EMPLEADOS
  JOHN CAMILO RANGEL DUQUE
 **************************************************************************/	


--CREAMOS LA SECUENCIA  DE LAS TABLAS 
create sequence sq_empleados;
create sequence sq_paises;
create sequence sq_tipo_identificaciones;
create sequence	sq_dominios;
create sequence sq_areas;

--CREAMOS LA TABLA DOMINIOS PARA LOS CORREOS

create table dominios (
id_dmnio			number			default		sq_dominios.nextval
									constraint 	dominios_id_dmnio_pk			primary key
									constraint 	dominios_id_dmnio_nn			not null,
dscrpcion			varchar2(20)	constraint	dominios_dscrpcion_nn			not null			
);

comment on table    dominios                    is 'Tabla donde se registrará los dominios de correo de los empleados';
comment on column   dominios.id_dmnio          	is 'Llave primaria de cada dominio';
comment on column   dominios.dscrpcion          is 'Descripción del dominio del correo electronico';

-- CREAMOS LA TABLA PAISES 

create table paises	(
id_pais				number			default 	sq_paises.nextval
									constraint	paises_id_pais_pk				primary key
									constraint	paises_id_pais_nn				not null,
dscrpcion			varchar2(20)	constraint	paises_dscrpcion_nn				not null,
id_dmnio			number			constraint	paises_id_dmnio_fk				references dominios (id_dmnio)
									constraint	empleados_id_dmnio_nn			not null
);

comment on table    paises                    is 'Tabla donde se registrará el pais  de los empleados';
comment on column   paises.id_pais            is 'Llave primaria del pais ';
comment on column   paises.dscrpcion          is 'Descripción del pais';
comment on column   paises.id_dmnio    	  	is 'dominio correo del empleado';


-- CREAMOS LA TABLA TIPO DE IDENTICACIONES

create table tipo_identificaciones (
id_tpo_idntfccion	number			default 	sq_tipo_identificaciones.nextval
									constraint	tp_idntfccn_id_tpo_idntfccn_pk 	primary key
									constraint  tp_idntfccn_id_tpo_idntfccn_nn	not null,
dscrpcion			varchar2(20)	constraint 	tp_idntfccn_dscrpcion_nn		not null
);

comment on table    tipo_identificaciones       			 is 'Tabla donde se registrará los tipo de identificaciones de los empleados';
comment on column   tipo_identificaciones.id_tpo_idntfccion  is 'Llave primaria de los tipos de identificaciones';
comment on column   tipo_identificaciones.dscrpcion          is 'Descripción del dominio del tipo de identificación';


--CREAMOS LA TABLA AREAS DE TRABAJOS

create	table areas(
id_area				number			default 	sq_areas.nextval
									constraint	areas_id_area_pk				primary key
									constraint	areas_id_area_nn				not null,
dscrpcion			varchar2(20)	constraint	areas_dscrpcion_nn				not null
); 

comment on table    areas                    is 'Tabla donde se registrará donde fue contratado el empleado';
comment on column   areas.id_area          	 is 'Llave primaria de cada area de trabajo';
comment on column   areas.dscrpcion          is 'Descripción del area de trabajo';


--- CREAMOS TABLA EMPLEADOS
create table empleados (
id_empldo 			number 			default  	sq_empleados.nextval	
									constraint  empleados_id_empldo_pk 			primary key
									constraint	empleados_id_empldo_nn 			not null,
prmer_aplldo		varchar2(20) 	constraint	empleados_prmer_aplldo_nn		not null,
sgndo_aplldo		varchar2(20)	constraint 	empleados_sgndo_aplldo_nn		not null,
prmer_nmbre			varchar2(20)	constraint  empleados_prmer_nmbre_nn		not null,
otro_nmbre			varchar2(50)	default		null ,
id_pais 			number			constraint	empleados_id_pais_fk   			references paises (id_pais)
									constraint	empleados_id_pais_nn   			not null,
id_tpo_idntfccion	number 			constraint	empleados_id_tpo_idntfccion_fk	references tipo_identificaciones (id_tpo_idntfccion)
									constraint	empleados_id_tpo_idntfccion_nn	not null,
nmro_idntfccion		varchar2(20)	constraint 	empleados_nmro_idntfccion_nn	not null
								    constraint	empleados_nmro_idntfccion_ck	check	(regexp_like(nmro_idntfccion,'[[:alnum:]]+$')),
email				varchar2(300)	constraint	empleados_email_nn				not null
									constraint	empleados_email_un				unique,
fcha_ingrso			date			constraint	empleados_fcha_ingrso_nn		not null,
id_area				number			constraint	empleados_id_area_fk			references areas(id_area)
									constraint	empleados_id_area_nn			not null,
estado				varchar2(2)		default 	'S'
									constraint	empleados_estado_nn				not null
									constraint	empleados_estado_ck				check  (estado in ('S','N')),
fcha_rgstro			timestamp(6)	constraint	empleados_fcha_rgstro_nn		not null,	
fha_edcion			date 																					
);

comment on table    empleados                 	is 'Tabla donde se registrará la informacion de los empleados';
comment on column   empleados.id_empldo    	  	is 'Llave primaria de cada empleado registrado';
comment on column   empleados.prmer_aplldo    	is 'Primer apellido del empleado';
comment on column   empleados.sgndo_aplldo    	is 'Segundo apellido del empleado';
comment on column   empleados.prmer_nmbre    	is 'Primer Nombre del empleado';
comment on column   empleados.otro_nmbre    	is 'Segundo nombre  del empleado';
comment on column   empleados.id_pais    		is 'Pais del empleado';
comment on column   empleados.id_tpo_idntfccion is 'tipo de identificación del empleado';
comment on column   empleados.email    			is 'correo electronico del empleado';
comment on column   empleados.fcha_ingrso    	is 'fecha de ingreso electronico del empleado';
comment on column   empleados.id_area    		is 'area de  trabajo segun el contrato del empleado';
comment on column   empleados.estado    		is 'estado del empleado';
comment on column   empleados.fcha_rgstro    	is 'fecha de registro del empleado';












